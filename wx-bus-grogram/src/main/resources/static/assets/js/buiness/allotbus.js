$(document).ready(function () {
    //查找所有可用的汽车
    var body={startNum:1,num:500};
    $.ajax({
        type: 'POST',
        url: '/web/search/findCanUseBus',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var dropList = "";
            if(res.data.length<=0){
                dropList="<li class='selected'><a href='#'>无可用车辆</a></li> ";
            }else{
                $(res.data).each(function (index, item) {
                    dropList += "<li class='selected' data-busid='" + item.busId +"'><a href='#'>车牌号："+item.busId+"&nbsp;&nbsp;核载人数:" + item.seating + "</a></li>";
                })
            }
            $("ul.dropdown-menu.pull-right.station").append(dropList);

        }
    })

    //可用汽车的下拉框
    $(document).on("click","li.selected",function () {
       $("input[name='busId']").val($(this).data("busid"));

    })
    
    //提交更改
    
    $("button.btn.btn-info.btn-fill.btn-wd").click(function () {
        var data={};
        data.routeId=$("input[name='routeid']").val();
        data.busId=$("input[name='busId']").val();
        if(data.busId==""){
            alert("请选择车辆");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/web/update/allotbus',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                alert(res.data);
                $("button.btn.btn-info.btn-fill.btn-wd").attr("class","btn btn-info btn-fill btn-wd disabled")
                $("button.btn.btn-info.btn-fill.btn-wd").unbind();
            }
        })
    })

})