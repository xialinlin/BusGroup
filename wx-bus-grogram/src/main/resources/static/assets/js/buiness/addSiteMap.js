var map = new BMap.Map("modal-body");
// 创建地图实例
var point = new BMap.Point(113.291874,34.950196);
// 创建点坐标
map.centerAndZoom(point, 11);
// 初始化地图，设置中心点坐标和地图级别
var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
map.addControl(top_left_control);
map.addControl(top_left_navigation);

map.enableScrollWheelZoom(true);

map.addEventListener("click", function(e){
    var pt = e.point;
    var xy=pt.lng +", "+ pt.lat;
    $("input[name='stationCoord']").val(xy);
})