package com.wxbus.webController;

import com.wxbus.daomain.*;
import com.wxbus.service.*;
import com.wxbus.util.JacksonUtil;
import com.wxbus.util.MD5Util;
import com.wxbus.util.ResponseUtil;
import com.wxbus.util.TimeUtil;
import com.wxbus.wxController.WxLoginController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.spi.http.HttpContext;
import java.net.InetAddress;
import java.util.Date;

/**
 * @author: Demon
 * @date: 2018/5/31
 * @time: 20:29
 * Description:
 */
@Controller
@RequestMapping(value = "/web/add")
public class WebAddController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private DriverService driverService;
    @Autowired
    private RouteService routeService;
    @Autowired
    private BusService busService;
    @Autowired
    private StationService stationService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private PassengerService passengerService;
    private final Log logger = LogFactory.getLog(WebAddController.class);

    /**
     * @type method
     * @parameter [bus]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/11
     * @describe 添加汽车信息
     */
    @RequestMapping(value = "/addbus", method = {RequestMethod.POST})
    @ResponseBody
    public Object addBus(@RequestBody Bus bus) {
        logger.info("添加汽车信息");
        if (bus == null) {
            return ResponseUtil.fail();

        }
        busService.addBus(bus);
        return ResponseUtil.ok();

    }

    /**
     * @type method
     * @parameter [station]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/11
     * @describe 添加站点信息
     */
    @RequestMapping(value = "/addstation", method = {RequestMethod.POST})
    @ResponseBody
    public Object addStation(@RequestBody Station station) {

        logger.info("添加站点信息");
        if (station == null) {
            return ResponseUtil.fail();

        }
        stationService.addStation(station);
        return ResponseUtil.ok();

    }

    /**
     * @type method
     * @parameter [route, request]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/22
     * @describe 添加路线
     */
    @RequestMapping(value = "/addroute", method = {RequestMethod.POST})
    @ResponseBody
    public Object addRoute(@RequestBody Route route, HttpServletRequest request) {
        if (route == null) {
            return ResponseUtil.fail();
        }
        //Integer creatUser=Integer.parseInt(request.getSession().getAttribute("user").toString());
        Integer creatUser = -1;
        route.setCreatUser(creatUser);
        route.setCreatTime(new Date());

        //将传进来的字符串变成日期
        Date startRecruit=TimeUtil.getTimeByString(route.getStartRecruit(),"yyyy-MM-dd");
        Date endRecruit=TimeUtil.getTimeByString(route.getEndsRecruit(),"yyyy-MM-dd");
        //设置运行周期
        route.setRunTime(String.valueOf(TimeUtil.getDifDay(startRecruit,endRecruit)));
        logger.info(route.getRunTime());
        logger.info("由管理员添加线路");
        routeService.addRoute(route);

        return ResponseUtil.ok();
    }

    /**
     * @type method
     * @parameter [route, request]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/22
     * @describe 由管理员添加线路
     */
    @RequestMapping(value = "/addline", method = {RequestMethod.POST})
    @ResponseBody
    public Object addLine(@RequestBody Route route) {
        if (route == null) {
            return ResponseUtil.fail();
        }
        route.setCreatUser(0);
        route.setCreatTime(new Date());
        route.setRouteStatus(1);


        //将传进来的字符串变成日期
        Date startRecruit=TimeUtil.getTimeByString(route.getStartRecruit(),"yyyy-MM-dd");
        Date endRecruit=TimeUtil.getTimeByString(route.getEndsRecruit(),"yyyy-MM-dd");
        //设置运行周期
        route.setRunTime(String.valueOf(TimeUtil.getDifDay(startRecruit,endRecruit)));
        logger.info("由管理员添加线路");
        logger.info(route.getRunTime());
        routeService.addRoute(route);
        return ResponseUtil.ok();
    }

    /**
     * @type method
     * @parameter [driver]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/22
     * @describe 添加司机
     */
    @RequestMapping(value = "/addDriver", method = {RequestMethod.POST})
    @ResponseBody
    public Object addDriver(@RequestBody Driver driver, HttpServletRequest request) {
        if (driver == null) {
            return ResponseUtil.fail();
        }
        String ip = request.getHeader("x-forwarded-for");
        logger.info("IP" + ip);
        driver.setRegisterIp("192.168.1.1");

        String passwordMd5 = MD5Util.toMD5(driver.getDriverPassword());
        driver.setDriverPassword(passwordMd5);
        driver.setRegisterTime(new Date());
        logger.info("添加司机");
        driverService.addDriver(driver);
        return ResponseUtil.ok();
    }

    /**
     * @type method
     * @parameter [admin]
     * @back java.lang.Object
     * @author 如花
     * @creattime 2018/6/22
     * @describe 添加管理员
     */
    @RequestMapping(value = "/addAdmin", method = {RequestMethod.POST})
    @ResponseBody
    public Object addAdmin(@RequestBody Admin admin) {
        if (admin == null) {
            return ResponseUtil.fail();
        }
        String passwordMd5 = MD5Util.toMD5(admin.getAdminPassword());
        admin.setAdminPassword(passwordMd5);
        logger.info("添加管理员");
        adminService.addAdmin(admin);
        return ResponseUtil.ok();
    }


    @PostMapping("/addmsgcontent")
    @ResponseBody
    public Object addMessage(@RequestBody String body) {

        String title=JacksonUtil.parseString(body,"title");
        String receivertype=JacksonUtil.parseString(body,"receivertype");
        String receiver=JacksonUtil.parseString(body,"receiver");
        String content=JacksonUtil.parseString(body,"content");


        Message message=new Message();
        //设置默认信息
        message.setSender("管理员");
        message.setMark(0);
        message.setSendtime(new Date());
        message.setTitle(title);
        message.setReceivetype(receivertype);
        message.setContent(content);
        //通过 接收人类型，接收人信息查找自增主键

        logger.info("接受人类型"+receivertype);
        if( "1".equals(receivertype)){
            //根据司机账号查询自增主键
            Integer driverNum=driverService.findDriverById(receiver).getDriverNum();
            message.setReceivetype("司机");
            message.setReceiver(driverNum);
        }else if( "2".equals(receivertype)){
            Integer passengerId=passengerService.findMainKey(receiver);
            message.setReceivetype("乘客");
            message.setReceiver(passengerId);
        }else {
            logger.info("非法的接收人类型");
            return ResponseUtil.ok("非法的接收人类型");
        }
        messageService.insertMessage(message);
        return ResponseUtil.ok();
    }
}